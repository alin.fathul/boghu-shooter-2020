using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    public float speed;
    public GameObject ship;
    public Material material;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        material.mainTextureOffset += new Vector2(0, speed * Time.deltaTime);
        Vector3 position = transform.position;
        position.y += ship.GetComponent<ShipMovement>().upSpeed * Time.deltaTime;
        transform.position = position;
    }
}
