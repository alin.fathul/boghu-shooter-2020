using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBulletMovement : BulletMovement
{
    public float speed;

    private Movement movement;
    void Start()
    {
        movement = gameObject.GetComponent<Movement>();
    }

    public override void move()
    {
        movement.Velocity = new Vector3(0, speed, 0);
    }

    
}
