using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BulletMovement : MonoBehaviour
{
    public abstract void move();
}
