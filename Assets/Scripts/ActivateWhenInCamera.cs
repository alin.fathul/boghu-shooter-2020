using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateWhenInCamera : MonoBehaviour
{
    public float timeToActivate;

    private bool activated = false;

    private EnemyContainer enemyContainer;

    public bool Activated { get => activated; set => activated = value; }

    // Start is called before the first frame update
    void Start()
    {
        enemyContainer = Camera.main.GetComponent<EnemyContainer>();
        enemyContainer.addGameObject(this.gameObject);

        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
