using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    public Transform[] pathWaypoint;
    public float step;
    public bool drawGizmos;

    private List<Vector3> path = new List<Vector3>();
    private List<Vector3> pathVelocity = new List<Vector3>();

    private float[] dist;
    private float[] ratio;
    private float length;

    // Start is called before the first frame update
    void Start()
    {
        generatePath(step);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            for (int n = 0; n < pathWaypoint.Length; n += 4)
            {
                for (float i = step; i <= 1; i += step)
                {
                    Vector3 bezier = Mathf.Pow(1 - i, 3) * pathWaypoint[n].position + 3 * Mathf.Pow(1 - i, 2)
                                      * i * pathWaypoint[n + 1].position + 3 * (1 - i) * Mathf.Pow(i, 2) *
                                      pathWaypoint[n + 2].position + Mathf.Pow(i, 3) * pathWaypoint[n + 3].position;


                    Gizmos.DrawSphere(bezier, 1);
                }
            }
        }
    }

    private void generatePath(float step)
    {
        for (int n = 0; n < pathWaypoint.Length; n += 4)
        {
            for (float i = step; i <= 1; i += step)
            {
                Vector3 bezier = Mathf.Pow(1 - i, 3) * pathWaypoint[n].position + 3 * Mathf.Pow(1 - i, 2)
                                  * i * pathWaypoint[n + 1].position + 3 * (1 - i) * Mathf.Pow(i, 2) *
                                  pathWaypoint[n + 2].position + Mathf.Pow(i, 3) * pathWaypoint[n + 3].position;

                Vector3 bezierDerivative = 3 * Mathf.Pow(1 - i, 2) * (pathWaypoint[n + 1].position - pathWaypoint[n].position)
                                           + 6 * (1 - i) * i * (pathWaypoint[n + 2].position - pathWaypoint[n + 1].position) +
                                           3 * Mathf.Pow(i, 2) * (pathWaypoint[n + 3].position - pathWaypoint[n + 2].position);

                pathVelocity.Add(bezierDerivative);
                path.Add(bezier);
            }
        }

        dist = new float[path.Count];
        ratio = new float[path.Count];

        for (int i = 0; i < path.Count - 1; i++)
        {
            dist[i] = Vector3.Distance(path[i], path[i + 1]);
            length += dist[i];
        }

        for (int i = 0; i < path.Count - 1; i++)
        {
            ratio[i] = dist[i] / length;
        }
    }

    private float normalizeValue(float val, float max, float min)
    {
        return (val - min) / (max - min);
    }

    public Vector3 getDirection(float t)
    {
        float totalTime = 0;

        for (int i = 0; i < path.Count - 1; i++)
        {
            if (t >= totalTime && t <= totalTime + ratio[i])
            {
                Vector3 velocity = pathVelocity[i] + (pathVelocity[i + 1] - pathVelocity[i]) * normalizeValue(t, totalTime + ratio[i], totalTime);
                return velocity.normalized;
            }
            totalTime += ratio[i];
        }

        return pathVelocity[pathVelocity.Count - 1];
    }

    public Vector3 getPosition(float t)
    {
        float totalTime = 0;

        for (int i = 0; i < path.Count - 1; i++)
        {
            if (t >= totalTime && t <= totalTime + ratio[i])
            {
                Vector3 pos = path[i] + (path[i + 1] - path[i]) * normalizeValue(t, totalTime + ratio[i], totalTime);
                return pos;
            }
            totalTime += ratio[i];
        }

        return path[path.Count - 1];
    }

    public float getLength()
    {
        return length;
    }
}
