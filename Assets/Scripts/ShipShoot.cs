using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipShoot : MonoBehaviour
{
    public GameObject bullet;
    public Transform shootPosition;

    private float timer;
    private Bullet bulletComponent;

    // Start is called before the first frame update
    void Start()
    {
        bulletComponent = bullet.GetComponent<Bullet>();
    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;
        if (timer > bulletComponent.fireRate)
        {
            GameObject bullet = ObjectPool.SharedInstance.getPooledObject();
            if (bullet != null)
            {
                bullet.transform.position = shootPosition.transform.position;
                bullet.transform.rotation = transform.rotation;
                bullet.SetActive(true);
            }
            timer = 0;
        }
    }
}
