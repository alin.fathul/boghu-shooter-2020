using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyContainer : MonoBehaviour
{
    private List<GameObject> enemyList = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject gameObject in enemyList)
        {
            ActivateWhenInCamera active = gameObject.GetComponent<ActivateWhenInCamera>();
            if (!active.Activated)
            {
                if (Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z)).y >= gameObject.transform.position.y)
                {
                    StartCoroutine(activateObject(gameObject, gameObject.GetComponent<ActivateWhenInCamera>().timeToActivate));
                    active.Activated = true;
                }
            }
        }
    }

    public void addGameObject(GameObject gameObject)
    {
        this.enemyList.Add(gameObject);
    }

    IEnumerator activateObject(GameObject gameObject, float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(true);
        enemyList.Remove(gameObject);
    }
}
