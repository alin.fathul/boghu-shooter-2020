using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    public float upSpeed;
    public float maxSpeed;
    public float mass;
    public float maxForce;
    public float slowingRadius;
    private Vector3 targetPosition;
    private Movement movement;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        movement = GetComponent<Movement>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                Vector3 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10));
                targetPosition = worldPos;
            }
        }*/

        
        Vector2 screenPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
        targetPosition = worldPosition;

        Vector3 steeringForce = Vector3.ClampMagnitude(getArrivalSteering(), maxForce);
        Vector3 acceleration = steeringForce / mass;
        movement.Velocity = Vector3.ClampMagnitude(movement.Velocity + acceleration, maxSpeed);

        transform.Translate(new Vector3(0, upSpeed * Time.deltaTime, 0));
        targetPosition.y += upSpeed * Time.deltaTime;

        animator.SetInteger("xVelocity", (int)movement.Velocity.x);
    }

    public Vector3 getArrivalSteering()
    {
        Vector3 desired_velocity = targetPosition - transform.Find("TargetPosition").gameObject.transform.position;
        float distance = Vector3.Magnitude(desired_velocity);

        if (distance < slowingRadius)
        {
            desired_velocity = Vector3.Normalize(desired_velocity) * maxSpeed * (distance / slowingRadius);
}
        else
        {
            desired_velocity = Vector3.Normalize(desired_velocity) * maxSpeed;
        }

        Vector3 steering = desired_velocity - movement.Velocity;

        return steering;
    }
}
