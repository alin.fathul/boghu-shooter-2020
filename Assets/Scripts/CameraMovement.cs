using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject ship;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LateUpdate()
    {
        Vector3 position = transform.position;
        position.y += ship.GetComponent<ShipMovement>().upSpeed * Time.deltaTime;
        transform.position = position;
    }
}
