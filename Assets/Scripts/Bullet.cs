using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float fireRate;
    public float damage;
    public float destroyTimer;
    public GameObject bulletExplosion;

    private BulletMovement bulletMovement;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        bulletMovement = gameObject.GetComponent<BulletMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        bulletMovement.move();

        timer += Time.deltaTime;

        if (timer > destroyTimer)
        {
            gameObject.SetActive(false);
            timer = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Instantiate(bulletExplosion, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
        timer = 0;
    }
}
