using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    public float speed;
    public GameObject pathObject;
    public float tEnd = 1;

    private Path path;
    private float t;

    private Vector3 startPosition;
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        path = pathObject.GetComponent<Path>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = path.getPosition(t) - (pathObject.transform.GetChild(0).transform.position - startPosition);
        if (t <= tEnd)
        {
            t += speed * Time.deltaTime / path.getLength();
        }
        Vector3 dir = path.getDirection(t);
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        spriteRenderer.color = Color.red;
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }
        StartCoroutine(spriteColorWhite());    
    }

    IEnumerator spriteColorWhite()
    {
        yield return new WaitForSeconds(0.05f);
        spriteRenderer.color = Color.white;
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
}
